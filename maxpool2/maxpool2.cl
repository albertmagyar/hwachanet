__kernel
void maxpool2(global const float *a,
              global float *b,
              int na) {
  int nb = na / 2;
  int id = get_global_id(0);
  if (id < nb) {
    for (int i = 0; i < nb; i++) {
      float m1 = max(a[2*i*na+2*id],a[2*i*na+2*id+1]);
      float m2 = max(a[(2*i+1)*na+2*id],a[(2*i+1)*na+2*id+1]);
      b[i*nb+id] = max(m1,m2);
    }
  }
}
