#define __CL_ENABLE_EXCEPTIONS

#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <opencl/cl.hpp>
#include <opencl/opencl.h>
#include "maxpool-naive.h"

#define THRESHOLD 0.00001

using namespace std;

int main () {
  vector<cl::Platform> platforms;
  vector<cl::Device> devices;
  vector<cl::Kernel> kernels;
  try {
    cl::Platform::get(&platforms);
    platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);
    cl::Context context(devices);
    cl::CommandQueue queue(context, devices[0]);
    ifstream cl_file("maxpool2.cl");
    string cl_string(istreambuf_iterator<char>(cl_file), (istreambuf_iterator<char>()));
    cl::Program::Sources source(1, make_pair(cl_string.c_str(), 
                                             cl_string.length() + 1));
    cl::Program program(context, source);

    program.build(devices);
    cl::Kernel kernel(program, "maxpool2");

    size_t na = 1024;
    size_t nb = na / 2;
    
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(-1.0,1.0);
    auto pos_neg_rand = std::bind (distribution, generator);
    
    vector<float> a(na*na);
    for (auto it = a.begin(); it != a.end(); it++)
      *it = pos_neg_rand();

    cl::Buffer abuf(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                    sizeof(float) * na * na, a.data());
    cl::Buffer bbuf(context, CL_MEM_READ_WRITE,
		    sizeof(float) * nb * nb, NULL);

    kernel.setArg(0, abuf);
    kernel.setArg(1, bbuf);
    kernel.setArg(2, na);
 
    // execute kernel
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(nb), cl::NDRange(1,1), NULL);
    vector<float> b(nb*nb);
    queue.enqueueReadBuffer(bbuf, CL_TRUE, 0, sizeof(float) * nb * nb, b.data());
    
    // wait for completion
    queue.finish();

    vector<float> bref(nb*nb);
    maxpool_naive(a.data(),bref.data(),na,2);
    for (int i = 0; i < bref.size(); i++) {
      if (fabs(bref[i]-b[i]) > THRESHOLD) {
        cerr << "Kernel produced incorrect results!" << endl;
        exit(-1);
      }
    }
  } catch (cl::Error e) {
    cout << endl << e.what() << " : " << e.err() << endl;
    return -1;
  }
     
  return 0;
     
}
