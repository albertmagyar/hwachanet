#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void maxpool_naive(const float *a, float *b, size_t na, int factor) {
  size_t nb = na / factor;
  for (int i = 0; i < nb; i++) {
    for (int j = 0; j < nb; j++) {
      b[i*nb + j] = a[i*factor*na + j*factor];
      for (int ii = 0; ii < factor; ii++)
        for (int jj = 0; jj < factor; jj++)
          b[i*nb + j] = fmax(b[i*nb + j],a[(i*factor+ii)*na + j*factor+jj]);
    }
  }
}

