__kernel
void conv2k3_unroll(global const float *a,
                    global const float *b,
                    global float *c,
                    int na) {
  int nb = 3;
  int nc = na - nb + 1;
  int id = get_global_id(0);
  if (id < nc) {
    private float sub00 = a[0 * na + 0 + id];
    private float sub01 = a[0 * na + 1 + id];
    private float sub02 = a[0 * na + 2 + id];
    private float sub10 = a[1 * na + 0 + id];
    private float sub11 = a[1 * na + 1 + id];
    private float sub12 = a[1 * na + 2 + id];
    private float sub20 = a[2 * na + 0 + id];
    private float sub21 = a[2 * na + 1 + id];
    private float sub22 = a[2 * na + 2 + id];
    for (int i = 0; i < nc; i++) {
      c[i * nc + id] =
        sub00 * b[0 * nb + 0] +
        sub01 * b[0 * nb + 1] +
        sub02 * b[0 * nb + 2] +
        sub10 * b[1 * nb + 0] +
        sub11 * b[1 * nb + 1] +
        sub12 * b[1 * nb + 2] +
        sub20 * b[2 * nb + 0] +
        sub21 * b[2 * nb + 1] +
        sub22 * b[2 * nb + 2];
      sub00 = sub10;
      sub01 = sub11;
      sub02 = sub12;
      sub10 = sub20;
      sub11 = sub21;
      sub12 = sub22;
      sub20 = a[(2 + i + 1) * na + 0 + id];
      sub21 = a[(2 + i + 1) * na + 1 + id];
      sub22 = a[(2 + i + 1) * na + 2 + id];
    }
  }
}
