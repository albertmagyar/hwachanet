#define __CL_ENABLE_EXCEPTIONS

#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <opencl/cl.hpp>
#include <opencl/opencl.h>
#include "conv2-naive.h"

#define THRESHOLD 0.00001

using namespace std;

int main () {
  vector<cl::Platform> platforms;
  vector<cl::Device> devices;
  vector<cl::Kernel> kernels;
  try {
    cl::Platform::get(&platforms);
    platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);
    cl::Context context(devices);
    cl::CommandQueue queue(context, devices[0]);
    ifstream cl_file("conv2k3-unroll.cl");
    string cl_string(istreambuf_iterator<char>(cl_file), (istreambuf_iterator<char>()));
    cl::Program::Sources source(1, make_pair(cl_string.c_str(), 
                                             cl_string.length() + 1));
    cl::Program program(context, source);

    program.build(devices);
    cl::Kernel kernel(program, "conv2k3_unroll");

    size_t na = 1024;
    size_t nb = 3;
    size_t nc = na - nb + 1;

    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(0.0,1.0);
    auto unit_rand = std::bind (distribution, generator);
    
    vector<float> a(na*na);
    for (auto it = a.begin(); it != a.end(); it++)
      *it = unit_rand();

    vector<float> b(nb*nb);
    for (auto it = b.begin(); it != b.end(); it++)
      *it = unit_rand();

    cl::Buffer abuf(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                    sizeof(float) * a.size(), a.data());
    cl::Buffer bbuf(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		    sizeof(float) * b.size(), b.data());
    cl::Buffer cbuf(context, CL_MEM_READ_WRITE,
                    sizeof(float) * nc * nc, NULL);

    // set message as kernel argument
    kernel.setArg(0, abuf);
    kernel.setArg(1, bbuf);
    kernel.setArg(2, cbuf);
    kernel.setArg(3, na);
 
    // execute kernel
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(nc), cl::NDRange(1,1), NULL);
    vector<float> c(nc*nc);
    queue.enqueueReadBuffer(cbuf, CL_TRUE, 0, sizeof(float) * c.size(), c.data());
    
    // wait for completion
    queue.finish();

    vector<float> cref(nc*nc);
    conv2_naive(a.data(),b.data(),cref.data(),na,nb);
    for (int i = 0; i < cref.size(); i++) {
      if (fabs(cref[i]-c[i]) > THRESHOLD) {
        cerr << "Kernel produced incorrect results!" << endl;
        exit(-1);
      }
    }
  } catch (cl::Error e) {
    cout << endl << e.what() << " : " << e.err() << endl;
    return -1;
  }
     
  return 0;
     
}
