#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void conv2_naive(const float *a, const float *b, float *c, size_t na, size_t nb) {
  size_t nc = na - nb + 1;
  size_t offset = nb / 2;
  memset(c, 0, nc * nc * sizeof(float));
  for (size_t i = 0; i < nc; i++)
    for (size_t j = 0; j < nc; j++)
      for (size_t ii = 0; ii < nb; ii++)
        for (size_t jj = 0; jj < nb; jj++)
          c[i*nc + j] += a[(i+ii)*na + j+jj] * b[ii*nb + jj];
}
