import sys

ksize = int(sys.argv[1])

print '''
__kernel
void conv2k{0}_unroll(global const float *a,
                      global const float *b,
                      global float *c,
                      int na) {{
  int nb = {0};
  int nc = na - nb + 1;
  int id = get_global_id(0);
  if (id < nc) {{
'''.format(ksize)

for i in range(ksize):
    for j in range(ksize):
        print "private float sub{0}{1} = a[{0} * na + {1} + id];".format(i,j)

print "for (int i = 0; i < nc; i++) {"
print "c[i * nc + id] ="

for i in range(ksize):
    for j in range(ksize):
        if (i == ksize - 1 and j == ksize - 1):
            print "sub{0}{0} * b[{0} * nb + {0}];".format(i)
        else:
            print "sub{0}{1} * b[{0} * nb + {1}] +".format(i,j)

for i in range(ksize-1):
    for j in range(ksize):
        print "sub{0}{1} = sub{2}{1};".format(i,j,i+1)

for j in range(ksize):
    print "sub{0}{1} = a[({0} + i + 1) * na + {1} + id];".format(ksize-1,j)

print "}\n}\n}\n"

