__kernel
void relu(global const float *a,
          global float *b,
          int len) {
  int id = get_global_id(0);
  if (id < len)
    b[id] = max((float) 0.0,a[id]);
}
