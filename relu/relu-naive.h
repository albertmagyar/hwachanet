#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void relu_naive(const float *in, float *out, size_t len) {
  for (size_t i = 0; i < len; i++)
    out[i] = fmax(in[i],0);
}
