__kernel
void conv2(global const float *a,
           global const float *b,
           global float *c,
           int na,
	   int nb) {
  int nc = na - nb + 1;
  int id0 = get_global_id(0);
  int id1 = get_global_id(1);
  if (id0 < nc && id1 < nc) {
    float sum = 0.0;
    for (int i = 0; i < nb; i++)
      for (int j = 0; j < nb; j++)
        sum += a[(j + id1)*na + i + id0] * b[j*nb + i];
    c[id1*nc + id0] = sum;
  }
}
