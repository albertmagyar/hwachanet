class ConvPoolRelu {
  size_t input_n;
  size_t input_height;
  size_t kernel_n;
  size_t n_kernels;
  size_t downsample_factor;

  cl4d conv_weights;
  vector<float> biases;

  void process(cl3d const &input) {
    cl3d cout();
    cl3d out()
    for (int oidx = 0; oidx < n_kernels; oidx++) {
      omapweights = convweights[oidx];
      obuf = cout[oidx];
      for (int iidx = 0; iidx < input_height; iidx++) {
	weights = omapweights[iidx];
	imap = input[iidx];
	conv2accum(imap,weights,obuf,input_n,kernel_n);
      }
    }
    for (int oidx = 0; oidx < n_kernels; oidx++) {
      maxpool2(cout[oidx],out[oidx],cout_n);
      addvs(out[oidx],biases[oidx],out[oidx].size());
      relu(out[oidx],out[oidx],out[oidx].size());
    }
    return out;
  }
