from __future__ import division
import matplotlib
import numpy as np
from scipy import io
from sklearn import svm
from sklearn.metrics import confusion_matrix
from pylab import imshow, show, get_cmap
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 16})
data = io.loadmat("data/digit_dataset/train.mat")

params = {
    "nsamples": len(data["train_labels"]),
    "vsize": 10000
}

perm = np.random.permutation(params["nsamples"])
pix = data["train_images"].ravel(order = "F") \
                          .reshape((-1,28,28)) \
                          .transpose((0,2,1)) \
                        .reshape((-1,28*28))[perm] + 0.0
labels = data["train_labels"].ravel()[perm]

vsize = params["vsize"]
tx = pix[:-vsize]
ty = labels[:-vsize]
vx = pix[-vsize:]
vy = labels[-vsize:]

class HiddenLayer:
    def __init__(self,osize,isize,next_size):
        rand_factor = np.sqrt(6.0 / (isize + next_size))
        self.weights = np.random.random_sample((osize,isize)) * (rand_factor * 2) - rand_factor
        self.biases = np.random.random_sample((osize,)) * (rand_factor * 2) - rand_factor

    def forward(self,prev):
        self.prev = prev
        self.out = np.dot(self.weights,prev) + self.biases
        return self.out

    def backward(self,de_do):
        # gradient of output wrt input
        tanhprime = 1 - np.power(self.out,2) # tanh' = 1 - tanh^2
        deltas = tanhprime * de_do
        de_di = np.dot(self.weights.T,deltas)
        de_dw = np.dot(deltas,self.prev.T)
        de_db = deltas
        return de_di, de_dw, de_db

class LogisticOutputLayer:
    def __init__(self,osize,isize):
        self.weights = np.zeros((osize,isize))
        self.biases = np.zeros((osize,))

    def softmax(self,vec):
        evec = np.exp(vec)
        self.out = evec / np.sum(evec)
        return self.out

    def forward(self,prev):
        self.prev = prev
        self.out = self.softmax(np.dot(self.weights,prev) + self.biases)

    def backward(self,y):
        deltas = y - self.out
        de_di = np.dot(self.weights.T,deltas)
        de_dw = np.dot(deltas,self.prev.T)
        de_db = deltas
        return de_di, de_dw, de_db

h_layer = HiddenLayer(osize=500, isize=28*28, next_size=10)
o_layer = LogisticOutputLayer(osize=10, isize=500)

while (True):
    ridx = np.random.randint(ty.size)
    i = tx[ridx]
    h = h_layer.forward(i)
    o = o_layer.forward(h)
    
    break
