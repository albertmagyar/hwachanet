
__kernel
void conv2k5_unroll(global const float *a,
                      global const float *b,
                      global float *c,
                      int na) {
  int nb = 5;
  int nc = na - nb + 1;
  int id = get_global_id(0);
  if (id < nc) {

private float sub00 = a[0 * na + 0 + id];
private float sub01 = a[0 * na + 1 + id];
private float sub02 = a[0 * na + 2 + id];
private float sub03 = a[0 * na + 3 + id];
private float sub04 = a[0 * na + 4 + id];
private float sub10 = a[1 * na + 0 + id];
private float sub11 = a[1 * na + 1 + id];
private float sub12 = a[1 * na + 2 + id];
private float sub13 = a[1 * na + 3 + id];
private float sub14 = a[1 * na + 4 + id];
private float sub20 = a[2 * na + 0 + id];
private float sub21 = a[2 * na + 1 + id];
private float sub22 = a[2 * na + 2 + id];
private float sub23 = a[2 * na + 3 + id];
private float sub24 = a[2 * na + 4 + id];
private float sub30 = a[3 * na + 0 + id];
private float sub31 = a[3 * na + 1 + id];
private float sub32 = a[3 * na + 2 + id];
private float sub33 = a[3 * na + 3 + id];
private float sub34 = a[3 * na + 4 + id];
private float sub40 = a[4 * na + 0 + id];
private float sub41 = a[4 * na + 1 + id];
private float sub42 = a[4 * na + 2 + id];
private float sub43 = a[4 * na + 3 + id];
private float sub44 = a[4 * na + 4 + id];
for (int i = 0; i < nc; i++) {
c[i * nc + id] =
sub00 * b[0 * nb + 0] +
sub01 * b[0 * nb + 1] +
sub02 * b[0 * nb + 2] +
sub03 * b[0 * nb + 3] +
sub04 * b[0 * nb + 4] +
sub10 * b[1 * nb + 0] +
sub11 * b[1 * nb + 1] +
sub12 * b[1 * nb + 2] +
sub13 * b[1 * nb + 3] +
sub14 * b[1 * nb + 4] +
sub20 * b[2 * nb + 0] +
sub21 * b[2 * nb + 1] +
sub22 * b[2 * nb + 2] +
sub23 * b[2 * nb + 3] +
sub24 * b[2 * nb + 4] +
sub30 * b[3 * nb + 0] +
sub31 * b[3 * nb + 1] +
sub32 * b[3 * nb + 2] +
sub33 * b[3 * nb + 3] +
sub34 * b[3 * nb + 4] +
sub40 * b[4 * nb + 0] +
sub41 * b[4 * nb + 1] +
sub42 * b[4 * nb + 2] +
sub43 * b[4 * nb + 3] +
sub44 * b[4 * nb + 4];
sub00 = sub10;
sub01 = sub11;
sub02 = sub12;
sub03 = sub13;
sub04 = sub14;
sub10 = sub20;
sub11 = sub21;
sub12 = sub22;
sub13 = sub23;
sub14 = sub24;
sub20 = sub30;
sub21 = sub31;
sub22 = sub32;
sub23 = sub33;
sub24 = sub34;
sub30 = sub40;
sub31 = sub41;
sub32 = sub42;
sub33 = sub43;
sub34 = sub44;
sub40 = a[(4 + i + 1) * na + 0 + id];
sub41 = a[(4 + i + 1) * na + 1 + id];
sub42 = a[(4 + i + 1) * na + 2 + id];
sub43 = a[(4 + i + 1) * na + 3 + id];
sub44 = a[(4 + i + 1) * na + 4 + id];
}
}
}

